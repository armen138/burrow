/**
 * This is an example server using Burrow as a framework.
 * You can use this as a base, or do your own thing.
 */
 import Burrow from "../burrow/Burrow.js";
import fs from "fs";

 // Create Gemini Burrow server, listen on port 3000 with max 30 connections
 let gemini = new Burrow.Gemini(3000, 30);
 // Create Spartan Burrow server, listen on port 3001 with max 30 connections
 let spartan = new Burrow.Spartan(3001, 30);
 // Create main router
 let router = new Burrow.Router();
 // Add the basic routes for server root
 router.add("/index", request => new Burrow.Response().redirect("/"));
 // Serve a file on the root path
 router.add("/", request => new Burrow.Response().file("public/index.gmi"));
 // Serve an asynchronous route
 router.add("/async", request => {
     let promise = new Promise((resolve, reject) => {
        fs.readFile("public/index.gmi", (err, data) => {
            if(err) {
                reject(err);
            } else {
                resolve(new Burrow.Response(data));
            }
        });      
     });
     return promise;
 });
 // Add a route where we take input and echo it back
 router.add("/send_text", request => {
     console.log(request);     
     if (request.body) {
         let response_text = `You sent me:\n${request.body}`;
         if (request.token && request.token != "") {
             response_text = `${response_text}\nWith token: ${request.token}`;
         }
         return new Burrow.Response(response_text);
     }
     return new Burrow.Response().input("/send_text", "send me something");
 });
 
 router.add("/magic/:spell", request => {
     return new Burrow.Response(`> Incantation: ${request.spell}`);
 });
 router.add("/hello/:greeting/hi", request => {
     return new Burrow.Response(`> Greeting: ${request.greeting}`);
 });
 // Create a secondary router to server our SLOG (spartan log, right? right?)
 let slog_router = new Burrow.Router();
 slog_router.add("/article", request => {
     return new Burrow.Response("This is an article, read it");
 });
 // Serve some static files, too!
 let static_router = new Burrow.StaticRouter("public");
 let api_router = new Burrow.StaticRouter("api");
 // Mount all our routers on gemini server
 gemini.mount("/", router);
 gemini.mount("/", static_router);
 gemini.mount("/slog", slog_router);
 gemini.mount("/api", api_router);
 // Mount all our routers on spartan server
 spartan.mount("/", router);
 spartan.mount("/", static_router);
 spartan.mount("/slog", slog_router);
 spartan.mount("/api", api_router);

 // Wait and listen.
 gemini.listen();
 spartan.listen();

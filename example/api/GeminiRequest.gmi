# Class GeminiRequest

GeminiRequest class

## GeminiRequest(data)

Create a Request object from a client request

### parameters
``` table
┌─────┬───────┬─────────────┬─────────────────────────────────┐
│name │type   │defaultvalue │description                      │
├─────┼───────┼─────────────┼─────────────────────────────────┤
│data │buffer │             │Something we got from the client │
└─────┴───────┴─────────────┴─────────────────────────────────┘
```


## Class Index

=> Burrow.gmi Burrow: Main Burrow class
=> Gemini.gmi Gemini: Gemini Server class
=> GeminiRequest.gmi GeminiRequest: GeminiRequest class
=> GeminiResponse.gmi GeminiResponse: GeminiResponse class
=> Request.gmi Request: Request class
=> Response.gmi Response: Response class
=> Router.gmi Router: Router class, to route requests to responses
=> Server.gmi Server: Base Server class
=> Spartan.gmi Spartan: Spartan Server class
=> SpartanRequest.gmi SpartanRequest: SpartanRequest class
=> SpartanResponse.gmi SpartanResponse: SpartanResponse class
=> StaticRouter.gmi StaticRouter: StaticRouter class, for serving static files

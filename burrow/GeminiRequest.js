import Request from "./Request.js";

/** GeminiRequest class*/
class GeminiRequest extends Request {
    /**
     * Create a Request object from a client request
     * @param {buffer} data Something we got from the client
     */
     constructor(data, client_cert=null) {
        super(data);
        this.client_certificate = client_cert;
        if(data.toString().startsWith("gemini")) {
            let url = new URL(data.toString());
            this.host = url.host;
            this.path = url.pathname || "/";
            this.content_size = url.search.substr(1).length;
            this.body = decodeURIComponent(url.search.substr(1));    
        } else if (data.toString().startsWith("titan")) {
            let header = data.toString().split("\r\n")[0];
            let location = header.split(";")[0];
            let url = new URL(location);
            this.host = url.host;
            this.path = url.pathname || "/";
            let properties = Object.fromEntries(header.substr(location.length).split(";").filter(item => item != "").map(item => item.split("=")));
            if(properties.size) {
                this.content_size = properties.size;
            } else {
                // need size
                this.invalid = true;
            }
            if(properties.token) {
                this.token = properties.token;
            }
            this.body = data.toString().substr(header.length + 2);
        } else {
            this.invalid = true;
        }
    }
}

export default GeminiRequest;
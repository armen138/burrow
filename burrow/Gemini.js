import tls from "tls";
import fs from "fs";
import GeminiRequest from "./GeminiRequest.js";
import Server from "./Server.js";
import GeminiResponse from "./GeminiResponse.js";

/** Gemini Server class
 * @augments Server
*/
class Gemini extends Server {
    routers = {};
    /**
     * Create a Burrow server
     * @param {number} port The port to listen on
     * @param {number} max_connections How many connections to allow at once
     */
     constructor(port, max_connections) {
        super(port, max_connections);
        let options = {
            key: fs.readFileSync('private-key.pem'),
            cert: fs.readFileSync('public-cert.pem'),
            requestCert: true,
            rejectUnauthorized: false
        };
        this.server = tls.createServer(options, async (socket) => {
            let client_cert = socket.getPeerCertificate();
            socket.on('data', async (buffer) => {
                let request = new GeminiRequest(buffer, client_cert);
                let response = false;
                if (request.invalid) {
                    if (buffer.toString().startsWith("gemini://") || buffer.toString().startsWith("titan://")) {
                        response = new GeminiResponse().fail('Bad Request', GeminiResponse.BAD_REQUEST);
                    } else {
                        response = new GeminiResponse().fail('Proxy Request Refused', GeminiResponse.PROXY_REFUSED);
                    }
                } else {
                    try {
                        console.log(socket.remoteAddress, 'port', socket.remotePort, request.path);
                        response = await this.routeRequest(request);
                        if (!response) {
                            response = new GeminiResponse().fail("Not found");
                        }
                    } catch (e) {
                        response = new GeminiResponse().fail(`Server error: ${e}`, GeminiResponse.SERVER_ERROR);
                    }    
                }
                try {
                    socket.end(response.serialize());
                } catch(e) {
                    console.error("Failed to respond to client: " + e.message);
                }
            });
            socket.on('error', err => {
                console.error("Socket error", err);
            });
        });
        this.server.maxConnections = max_connections;
    }
    /** Listen on this.port */
    listen() {
        this.server.listen(this.port);
    }
}

export default Gemini;
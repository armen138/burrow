/** Router class, to route requests to responses */
class Router {
    /**
     * Create a Router instance.
     * We can use this to route things!
     */
    constructor() {
        this.routes = {};
    }
    /**
     * Add a route to this router.
     * @param {string} path Path to listen for
     * @param {Function(Request)} callback function to call when we hit this route
     */
    add(path, callback) {
        this.routes[path] = callback;
    }
    /**
     * Try resolving a request on this router.
     * @param {Request} request Original request object that triggered this route
     * @param {string} route_path The normalized route path in the scope of this router
     * @returns {Response|false} Returns false if this router can't match the requested resource
     */
    resolve(request, route_path) {
        for (let path in this.routes) {
            if (path == route_path || path == "/" + route_path) {
                return this.routes[path](request);
            }
            if(path.includes(":")) {
                let tokens = path.split("/").filter(item => item != "");
                let route_tokens = route_path.split("/").filter(item => item != "");
                let params = {};
                let match = true;
                if(tokens.length == route_tokens.length) {
                    for(let i = 0; i < tokens.length; i++) {
                        if(tokens[i].startsWith(":")) {
                            params[tokens[i].substr(1)] = route_tokens[i];
                        } else {
                            if(tokens[i] != route_tokens[i]) {
                                match = false;
                                break;
                            }
                        }
                    }
                    if(match) {
                        Object.assign(request, params);
                        let response = this.routes[path](request);
                        if(response) return response;
                    }
                }
            }
        }
        return false;
    }
}

export default Router;
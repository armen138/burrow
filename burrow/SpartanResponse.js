import Response from "./Response.js";

/** SpartanResponse class */
class SpartanResponse extends Response {
    static SERVER_ERROR = 5;
    static NOT_FOUNT = 4;
    static REDIRECT = 3;
    static OK = 2;
    constructor(body = "", mime = "text/gemini", charset = "utf-8") {
        super(body, mime, charset);
        this.status = SpartanResponse.OK;
    }
    from_generic_response(response, location = "/") {
        this.body = response.body;
        this.meta = response.meta;
        this.mime = response.mime;
        this.charset = response.charset;
        switch (response.status) {
            case Response.OK:
                this.status = SpartanResponse.OK;
                break;
            case Response.REDIRECT:
                this.status = SpartanResponse.REDIRECT;
                break;
            case Response.NOT_FOUND:
                this.status = SpartanResponse.NOT_FOUNT;
                break;
            case Response.SERVER_ERROR:
                this.status = SpartanResponse.SERVER_ERROR;
                break;
            case Response.REQUEST_INPUT:
                this.input(location, response.meta)
                break;
        }
        return this;
    }
    /**
     * Redirect to another location on this server
     * @param {string} location Redirect location. Should be an absolute path starting with /
     * @returns {Response} this
     */
     redirect(location) {
        this.status = SpartanResponse.REDIRECT;
        this.meta = location;
        return this;
    }
    /**
     * Respond with a request for input (Status 10)
     * @returns {Response} this
     */
     input(path, message) {
        this.status = SpartanResponse.OK;
        this.body = `=: ${path} ${message}`;
        return this;
    }
    /**
     * Create a failure response
     * @param {string} message Message to show for this failure
     * @param {number} status Status should be 4 for bad requests, and 5 for server errors
     * @returns {Response} this
     */
    fail(message, status = SpartanResponse.NOT_FOUND) {
        this.status = status;
        this.meta = message;
        return this;
    }
    /**
     * Serialize response so we can send it to the socket
     * @returns serialized response string
     */
    serialize() {
        let responseText = `${SpartanResponse.SERVER_ERROR} server error\r\n`;
        if (this.status == SpartanResponse.OK) {
            responseText = `${SpartanResponse.OK} ${this.mime}; charset=${this.charset}\r\n${this.body}`;
        } else {
            responseText = `${this.status} ${this.meta}\r\n`;
        }
        return responseText;
    }
}

export default SpartanResponse;
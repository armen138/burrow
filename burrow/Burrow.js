import Router from "./Router.js";
import StaticRouter from "./StaticRouter.js";
import Response from "./Response.js";
import Request from "./Request.js";
import Spartan from "./Spartan.js";
import Gemini from "./Gemini.js";

/** Main Burrow class */
class Burrow {
    static Router = Router;
    static StaticRouter = StaticRouter;
    static Response = Response;
    static Request = Request;
    static Spartan = Spartan;
    static Gemini = Gemini;
}

export default Burrow;

import Response from "./Response.js";

/** Base Server class */
class Server {
    constructor(port, max_connections) {
        this.port = port;
        this.max_connections = max_connections;
    }
    /**
     * routeRequest takes a request and tries to find a route to a response.
     * This should probably never be called manually, it is part of Burrow's request-flow.
     * @param {Request} request The request to route
     * @returns {Response|false} A Burrow.Response, or false if routing failed
     */
    async routeRequest(request) {
        if (!request.path) {
            return new Response().fail("Not found", Response.NOT_FOUND);
        }
        for (let prefix in this.routers) {
            if (request.path.startsWith(prefix)) {
                let path = request.path.replace(prefix, "");
                let response = false;
                for (let router of this.routers[prefix]) {
                    response = await router.resolve(request, path);
                    console.log(response);
                    if (response) break;
                }
                if (response) {
                    return response;
                }
            }
        }
        return false;
    }
    /**
     * Mount a router to a path on the server
     * @param {string} path A path prefix to mount the router
     * @param {Router} router An instance of Burrow.Router or Burrow.StaticRouter
     */
    mount(path, router) {
        this.routers[path] = this.routers[path] || []
        this.routers[path].push(router);
    }
}

export default Server;
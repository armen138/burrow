import fs from "fs";
import path from "path";

const KNOWN_MIMES = {
    ".gmi": "text/gemini",
    ".txt": "text/plain"
};

/** Response class */
class Response {
    static REQUEST_INPUT = 10;
    static REQUEST_SENSITIVE_INPUT = 11;
    static OK = 20;
    static REDIRECT = 30;
    static REDIRECT_PERMANENTLY = 31;
    static TEMPORARY_ERROR = 40;
    static SERVER_UNAVAILABLE = 41;
    static SERVER_ERROR = 42;
    static PROXY_ERROR = 43;
    static SLOW_DOWN = 44;
    static PERMANENT_FAILURE = 50;
    static NOT_FOUND = 51;
    static GONE = 52;
    static PROXY_REFUSED = 53;
    static BAD_REQUEST = 59;
    static CERTIFICATE_REQUIRED = 60;
    static CERTIFICATE_NOT_AUTHORIZED = 61;
    static CERTIFICATE_INVALID = 62;

    /**
     * Create a Response object
     * @param {string} body The body text to respond with
     * @param {string} mime The mime-type to assign to the response
     * @param {string} charset The charset we're enconding the response in
     */
    constructor(body = "", mime = "text/gemini", charset = "utf-8") {
        this.mime = mime;
        this.charset = charset;
        this.body = body;
        this.status = Response.OK;
        this.meta = "";
    }
    /**
     * Respond with raw data
     * @param {string} data Data to respond to client with. Must be complete with spartan header
     * @returns {Response} this
     */
    raw(data) {
        this._raw = data;
        return this;
    }
    /**
     * Redirect to another location on this server
     * @param {string} location Redirect location. Should be an absolute path starting with /
     * @returns {Response} this
     */
    redirect(location, status = Response.REDIRECT) {
        this.status = status;
        this.meta = location;
        return this;
    }
    /**
     * Respond with a request for input (Status 10)
     * @returns {Response} this
     */
    input(path, message) {
        this.status = Response.REQUEST_INPUT;
        this.meta = message;
        return this;
    }
    /**
     * Respond with a request for sensitive input (Status 11)
     * @returns {Response} this
     */
     sensitive_input(path, message) {
        this.status = Response.REQUEST_SENSITIVE_INPUT;
        this.meta = message;
        return this;
    }    
    /**
     * Create a failure response
     * @param {string} message Message to show for this failure
     * @param {number} status Status should be 4 for bad requests, and 5 for server errors
     * @returns {Response} this
     */
    fail(message, status = Response.NOT_FOUND) {
        this.status = status;
        this.meta = message;
        return this;
    }
    /**
     * Respond with the contents of a file
     * @param {string} filename The file we want to send back
     * @returns {Response} this
     */
    file(filename) {
        if (fs.existsSync(filename) && fs.statSync(filename).isFile()) {
            let content = fs.readFileSync(filename);
            this.body = content.toString();
            this.mime = KNOWN_MIMES[path.extname(filename)] || this.mime;
        } else {
            return false;
        }
        return this;
    }
    /**
     * Serialize response so we can send it to the socket
     * @returns serialized response string
     */
    serialize() {
        if(this._raw) {
            return this._raw;
        }
        let responseText = `${Response.SERVER_ERROR} server error\r\n`;
        if (this.status == Response.OK) {
            responseText = `${Response.OK} ${this.mime}; charset=${this.charset}\r\n${this.body}`;
        } else {
            responseText = `${this.status} ${this.meta}\r\n`;
        }
        return responseText;
    }
}

export default Response;
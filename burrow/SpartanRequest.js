import Request from "./Request.js";

/** SpartanRequest class */
class SpartanRequest extends Request {
    /**
     * Create a Request object from a client request
     * @param {buffer} data Something we got from the client
     */
    constructor(data) {
        super(data);
        let string_data = data.toString();
        let i = string_data.toString().indexOf('\r\n');
        let header = string_data.slice(0, i);
        let body = string_data.slice(i + 2);
        if (body.endsWith("\r\n")) {
            body = body.slice(0, body.length - 2);
        }
        this.invalid = false;
        let request = header.split(" ");
        if(request.length !== 3) {
            this.invalid = true;
        }
        this.host = request[0];
        this.path = request[1] || "/";
        this.content_size = parseInt(request[2], 10);
        if (isNaN(this.content_size)) {
            this.invalid = true;
        }
        this.body = body;
    }
}

export default SpartanRequest;
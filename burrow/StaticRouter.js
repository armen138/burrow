import path from "path";
import Response from "./Response.js";

/** StaticRouter class, for serving static files */
class StaticRouter {
    /**
     * Create static file Router
     * @param {string} path Path where to look for files
     */
    constructor(path) {
        this.path = path;
    }
    /**
     * Try to resolve requested file
     * @param {Request} request The request that triggered this router
     * @param {string} route_path The normalized path scoped for this router
     * @returns {Response|false} Returns false if router could not match requested resource
     */
    resolve(request, route_path) {
        let filename = path.join(this.path, route_path);
        let response = new Response().file(filename);
        return response;
    }
}

export default StaticRouter;
import Response from "./Response.js";

/** GeminiResponse class */
class GeminiResponse extends Response {

    /**
     * Create a GeminiResponse object
     * @param {string} body The body text to respond with
     * @param {string} mime The mime-type to assign to the response
     * @param {string} charset The charset we're enconding the response in
     */
    constructor(body = "", mime = "text/gemini", charset = "utf-8") {
        super(body, mime, charset);
    }
}

export default GeminiResponse;
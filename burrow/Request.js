/** Request class */
class Request {
    /**
     * Create a Request object from a client request
     * @param {buffer} data Something we got from the client
     */
    constructor(data) {
    }
}

export default Request;
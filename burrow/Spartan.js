import net from "net";
import SpartanRequest from "./SpartanRequest.js";
import Server from "./Server.js";
import SpartanResponse from "./SpartanResponse.js";

/** Spartan Server class
 * @augments Server
*/
class Spartan extends Server {
    routers = {};
    /**
     * Create a Burrow server
     * @param {number} port The port to listen on
     * @param {number} max_connections How many connections to allow at once
     */
     constructor(port, max_connections) {
        super(port, max_connections);
        this.server = net.createServer(socket => {
            socket.on('error', err => {
                console.error("Socket error", err);
            });
            socket.on('data', (buffer) => {
                let request = new SpartanRequest(buffer);
                let response = false;
                console.log(socket.remoteAddress, 'port', socket.remotePort, request.host, request.path, request.content_size);
                if (request.invalid) {
                    response = new SpartanResponse().fail(`Invalid request`, SpartanResponse.SERVER_ERROR);
                    return socket.end(response.serialize());
                }
                try {
                    response = this.routeRequest(request);
                    if(response.then) {
                        response.then(resp => {
                            socket.end(resp.serialize());
                        });
                    } else {
                        if (!response) {
                            response = new SpartanResponse().fail("Not found");
                        } else {
                            response = new SpartanResponse().from_generic_response(response, request.path);
                        }
                    }
                } catch (e) {
                    response = new SpartanResponse().fail(`Server error: ${e}`, SpartanResponse.SERVER_ERROR);
                }
                try {
                    if(!response.then) {
                        socket.end(response.serialize());
                    }
                } catch(e) {
                    console.error("Unable to respond to client ", e.message);
                }
            });
        });
        this.server.maxConnections = max_connections;
    }
    /** Listen on this.port */
    listen() {
        this.server.listen(this.port);
    }
}

export default Spartan;
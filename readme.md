
# Hello this is Burrow

Burrow is a spartan server implementation in Javascript.

Burrow is written in Javascript - but has *zero* dependencies outside of NodeJS.
No npm, yarn, weird unsafe dependencies, none of that. Just run and have fun.

## Why is it called "Burrow"?

I wrote a gemini/spartan client called *Gerbil*. It makes sense the server would be a place
all the gerbils get together for a rodent-party! Welcome to the burrow :)

## Ok but where can I find this stuff

=> https://gitlab.com/armen138/burrow Burrow on gitlab

=> https://gitlab.com/armen138/gerbil Gerbil on gitlab

# Running the example

There's an example included in the example folder. It showcases all of burrow's features, and serves both spartan and gemini.
In order to run it, you'll first need to generate certificates for the gemini server to use. You may do so with these commands:

``` bash
cd example
openssl genrsa -out private-key.pem 2048
openssl req -new -key private-key.pem -out csr.pem
openssl x509 -req -in csr.pem -signkey private-key.pem -out public-cert.pem
```

Then run the server in nodeJS as follows:

```
node index.js
```

